#include <stdio.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "mma8452.h"
#include "twi_eeprom.h"

/* USART */

static void usart_init(void)
{
	UBRR0 = F_CPU / (16 * 9600UL) - 1;
	UCSR0B = (0<<RXEN0)|(1<<TXEN0);
	UCSR0C = (3<<UCSZ00);
}

static int usart_putchar(char c)
{
	while (!(UCSR0A & (1<<UDRE0)))
		;
	UDR0 = c;
	return 0;
}

static void usart_puts(const char *s)
{
	if (s)
		while (*s)
			usart_putchar(*s++);
}

/* TWI */

static void twi_init(void)
{
#if defined(TWPS0)
	TWSR = 0;
#endif
	TWBR = F_CPU / (2 * 100e3) - 8;
}

/* ACCELEROMETER MMA8452 */

#ifndef OUTPUT_HUMAN_FRIENDLY
#define OUTPUT_HUMAN_FRIENDLY 0
#endif

static unsigned char mma8452_addr = MMA845X_ADDR;

#define mma845x_read_byte(r, d) \
	twi_eeprom_read_bytes(mma8452_addr, (r), 1, (d))

#define mma845x_write_byte(r, d) \
	twi_eeprom_write_bytes(mma8452_addr, (r), 1, (d))

static const char nibble2hex[] = "0123456789ABCDEF";

static void usart_puthex(const unsigned char v)
{
	usart_putchar(nibble2hex[(v >> 4) & 0xF]);
	usart_putchar(nibble2hex[v & 0xF]);
}

static unsigned char mma8452_findaddress(void)
{
	static const char str_mma845_prefix[] = "MMA845";

	unsigned char whoami;

	usart_puts(str_mma845_prefix);

	mma8452_addr = MMA845X_ADDR;
	if (mma845x_read_byte(MMA845X_WHO_AM_I, &whoami) != 1) {
		mma8452_addr = MMA845X_ADDR_SA0;
		if (mma845x_read_byte(MMA845X_WHO_AM_I, &whoami) != 1) {
			usart_puts("x not found.\r\n");
			return 0;
		}
	}
	switch (whoami) {
	case MMA845X_MMA8451Q_ID:
		whoami = '1';
		break;
	case MMA845X_MMA8452Q_ID:
		whoami = '2';
		break;
	case MMA845X_MMA8453Q_ID:
		whoami = '3';
		break;
	default:
		usart_puts("x not identified (0x");
		usart_puthex(whoami);
		usart_putchar(')');
		whoami = 0;
	}
	if (whoami)
		usart_putchar(whoami);
	usart_puts(" found @ address 0x");
	usart_puthex(mma8452_addr);
	usart_puts(".\r\n");

	return whoami;
}

static unsigned char mma8452_init(void)
{
	unsigned char ctrl_reg[5];

	if (!mma8452_findaddress()) {
		return 1;
	}

	if (mma845x_read_byte(MMA845X_CTRL_REG1, ctrl_reg) != 1) {
		goto init_failed;
	}

	if (ctrl_reg[0] & (1<<MMA845X_ACTIVE)) {
		ctrl_reg[0] &= ~(1<<MMA845X_ACTIVE);
		if (mma845x_write_byte(MMA845X_CTRL_REG1, ctrl_reg) != 1) {
			goto init_failed;
		}
	}

	ctrl_reg[0] = MMA845X_ODR_20MS;
	if (mma845x_write_byte(MMA845X_CTRL_REG1, ctrl_reg) != 1) {
		goto init_failed;
	}

	ctrl_reg[3] = (1<<MMA845X_INT_EN_DRDY);
	if (mma845x_write_byte(MMA845X_CTRL_REG4, ctrl_reg + 3) != 1) {
		goto init_failed;
	}

	/* Enable MMA8452 interrupts */
	EIMSK |= (1<<INT0);

	ctrl_reg[0] |= (1<<MMA845X_ACTIVE);
	if (mma845x_write_byte(MMA845X_CTRL_REG1, ctrl_reg) != 1) {
		goto init_failed;
	}

	return 0;

init_failed:
	usart_puts("MMA845X init failed.\r\n");
	return 1;
}

#if 0
#define mma845x_read_bytes(r, l, d) \
	twi_eeprom_read_bytes(mma8452_addr, (r), (l), (d))

static void print_odr_hp(void)
{
	unsigned char ctrl_reg[2];
	if (mma845x_read_bytes(MMA845X_CTRL_REG1, 2, ctrl_reg) != 2) {
		usart_puts("Error reading CTRL_REG.\r\n");
		return;
	}

	{
		unsigned char odr = (ctrl_reg[0] >> MMA845X_DR0) & 7;
		switch (odr) {
		case 7:
			odr = 1;
			break;
		case 6: case 5:
			odr = (800 >> odr) >> 1;
			break;
		default:
			odr = 800 >> odr;
			break;
		}
		fprintf(&usartfd, "ODR = %u Hz.\r\n", odr);
	}
}
#endif

static void print_xyz(const unsigned char *xyz_value)
{
	const unsigned char msb = xyz_value[0];
	const unsigned char lsb = xyz_value[1] >> 4;
	{
		const signed short value
			= ((signed short)(signed char)msb << 4) | lsb;
		char decascii[6];
		snprintf(decascii, 6, "%+5d", value);
		usart_puts(decascii);
	}
	usart_puts(" (0x");
	usart_puthex(msb);
	usart_putchar(nibble2hex[lsb & 0xF]);
	usart_putchar(')');
}

static unsigned char mma8452_get_xyz(void)
{
	unsigned char data[6];
	if (
		twi_eeprom_read_bytes(mma8452_addr,
			MMA845X_OUT_X_MSB, 6, data)
		!= 6
	) {
		usart_puts("Error reading output values.\r\n");
		return 1;
	}

	if (OUTPUT_HUMAN_FRIENDLY) {
		usart_putchar('{');
		print_xyz(&data[0]);
		usart_putchar(',');
		print_xyz(&data[2]);
		usart_putchar(',');
		print_xyz(&data[4]);
		usart_putchar('}');
		usart_putchar('\r');
	} else {
		unsigned char i = 0;
		while (i < 6) {
			usart_putchar(i == 0 ? '$' : ' ');
			usart_puthex(data[i]);
			usart_putchar(nibble2hex[data[i+1] >> 4]);
			i += 2;
		}
		usart_putchar('\r');
		usart_putchar('\n');
	}
	return 0;
}

static unsigned char mma8452_poll(void)
{
	unsigned char status;
	if (mma845x_read_byte(MMA845X_STATUS, &status) != 1) {
		usart_puts("Error reading STATUS.\r\n");
		return 1;
	}
	if (status & (1<<MMA845X_ZYXDR)) {
		return mma8452_get_xyz();
	}
	return 0;
}

/* GENERAL */

static void timer1_init(void)
{
	/* Normal operation timer */
	TCCR1A = 0;
	/* prescaler @ (2097 ms max) */
	TCCR1B = (F_CPU > 8000000UL) ? (4<<CS10)
		: (F_CPU > 2000000UL) ? (3<<CS10)
		: (2<<CS10);
	TIMSK1 = (1<<TOIE1);
}

static void ioinit(void)
{
	/* Needed by LED_BUILTIN */
	PORTB = 0x3F & ~(1<<PB5);
	DDRB |= (1<<PB5);

	/* Needed by TWI (I2C) */
	PORTC = 0x3F & ~((1<<PC4) | (1<<PC5));

	/* Needed by USART & INT0 */
	PORTD |= 0xFF & ~((1<<PD0) | (1<<PD1) | (1<<PD2));

	/* For MMA8452 interrupts */
	EICRA = 0;

	/* Disable digital input */
	/* ADC4D and ADC5D needed by TWI (I2C) */
	DIDR0 = (1<<ADC0D) | (1<<ADC1D) | (1<<ADC2D)
		| (1<<ADC3D) | (0<<ADC4D) | (0<<ADC5D);

	/* Disable analog comparators */
	ACSR |= (1<<ACD);
	DIDR1 = (1<<AIN0D) | (1<<AIN1D);

	/* Disable clocks for each unused modules */
	PRR = (1<<PRTIM2) | (1<<PRTIM0) | (1<<PRSPI) | (1<<PRADC);

	usart_init();
	twi_init();
	timer1_init();
}

static unsigned char mma8452_found = 0;

int main(void)
{
	ioinit();

	SMCR = (1<<SE);
	sei();
	while (1)
		sleep_cpu();

	return 0;
}

ISR(TIMER1_OVF_vect)
{
	PORTB |= (1<<PB5);

	if (!mma8452_found) {
		mma8452_found = !mma8452_init();
#if 0
		if (mma8452_found)
			print_odr_hp();
#endif
	}

	if (mma8452_found)
		mma8452_found = !mma8452_poll();

	PORTB &= ~(1<<PB5);
}

ISR(INT0_vect)
{
	unsigned char int_source;

	PORTB |= (1<<PB5);

	if (!mma8452_found) {
		EIMSK &= ~(1<<INT0);
		goto exit;
	}

	if (mma845x_read_byte(MMA845X_INT_SOURCE, &int_source) != 1) {
		mma8452_found = 0;
		goto exit;
	}

	/* Reset timer checker */
	TCNT1 = 0;

	if (int_source & (1<<MMA845X_SRC_DRDY))
		mma8452_get_xyz();

exit:
	PORTB &= ~(1<<PB5);
}
