#ifndef ACCEL_UTIL_H
#define ACCEL_UTIL_H

struct xyz_raw {
	signed short x;
	signed short y;
	signed short z;
};

struct xyz_g {
	double x;
	double y;
	double z;
};

double get_magnitude(const struct xyz_g *);
void raw_accel_to_g(struct xyz_g *, const struct xyz_raw *, int fullscale_g);
int poll_accel(struct xyz_raw *, FILE *in);

#endif
