#ifndef TWI_EEPROM_H
#define TWI_EEPROM_H

extern unsigned char twierrno;

unsigned char twi_eeprom_read_bytes(
	const unsigned char addr,
	const unsigned char reg,
	unsigned char len,
	unsigned char *out);

unsigned char twi_eeprom_write_bytes(
	const unsigned char addr,
	const unsigned char reg,
	unsigned char len,
	const unsigned char *in);

#endif
