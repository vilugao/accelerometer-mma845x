# Lista de afazeres

 - Aceitar o RX no lado do AVR, recebendo ajustes de configurações como ODR,
HPF, ASLP, etc. Lembre-se de tratar o recebimento em formato de texto.
 - Ajustar o zoom da tela de acordo com o Full Scale Range configurado no
MMA845x.
