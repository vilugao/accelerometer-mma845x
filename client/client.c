#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "accel_util.h"

static FILE *accelinput;

static volatile int interrupt_detected = 0;

static void sigint_handler(const int signum)
{
	(void)signum;
	interrupt_detected = 1;
}

static int parse_args(int argc, const char *argv[])
{
	if (argc >= 2) {
		accelinput = fopen(argv[1], "rb");
		if (accelinput == NULL) {
			perror(argv[1]);
			return 1;
		}
	}
	return 0;
}

int main(int argc, const char *argv[])
{
	struct xyz_raw xyzvalues_raw;
	struct xyz_g xyzvalues_g;

	accelinput = stdin;

	if (parse_args(argc, argv) != 0)
		return EXIT_FAILURE;

	if (signal(SIGINT, sigint_handler) == SIG_ERR)
		perror("signal");

	while (!interrupt_detected) {
		switch (poll_accel(&xyzvalues_raw, accelinput)) {
		case 0:
			continue;
		case -1:
			goto exit;
		}

		raw_accel_to_g(&xyzvalues_g, &xyzvalues_raw, 2);

		printf(
		"\r{%+5hd, %+5hd, %+5hd} {%+5.2f, %+5.2f, %+5.2f} = %+5.2f.",
			xyzvalues_raw.x, xyzvalues_raw.y, xyzvalues_raw.z,
			xyzvalues_g.x, xyzvalues_g.y, xyzvalues_g.z,
			get_magnitude(&xyzvalues_g));
	}
exit:
	puts("\nQuiting...");
	fclose(accelinput);
	return EXIT_SUCCESS;
}
