#include <util/twi.h>

#include "twi_eeprom.h"

unsigned char twierrno = 0;

static unsigned char twi_eeprom_select_register(
	const unsigned char addr,
	const unsigned char reg)
{
begin:
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	switch (TW_STATUS) {
	case TW_REP_START:
	case TW_START:
		break;
	case TW_MT_ARB_LOST:
		goto begin;
	default:
		twierrno = 1;
		return 1;
	}

	TWDR = addr | TW_WRITE;
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	switch (TW_STATUS) {
	case TW_MT_SLA_ACK:
		break;
	case TW_MT_ARB_LOST:
		goto begin;
	default:
		twierrno = 2;
		goto error;
	}

	TWDR = reg;
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	switch (TW_STATUS) {
	case TW_MT_DATA_ACK:
		break;
	case TW_MT_ARB_LOST:
		goto begin;
	default:
		twierrno = 3;
		goto error;
	}

	return 0;
error:
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
	return 1;
}

unsigned char twi_eeprom_read_bytes(
	const unsigned char addr,
	const unsigned char reg,
	unsigned char len,
	unsigned char *out)
{
	unsigned char bytes = 0;
begin:
	if (twi_eeprom_select_register(addr, reg) != 0)
		goto quit;

	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	switch (TW_STATUS) {
	case TW_REP_START:
	case TW_START:
		break;
	case TW_MR_ARB_LOST:
		goto begin;
	default:
		twierrno = 4;
		goto stop;
	}

	TWDR = addr | TW_READ;
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	switch (TW_STATUS) {
	case TW_MR_SLA_ACK:
		break;
	case TW_MR_ARB_LOST:
		goto begin;
	default:
		twierrno = 5;
		goto stop;
	}

	for (; len > 0; len--) {
		TWCR = (1<<TWINT) | (1<<TWEN) | (len > 1 ? (1<<TWEA) : 0);
		while (!(TWCR & (1<<TWINT)))
			;
		switch (TW_STATUS) {
		case TW_MR_DATA_NACK:
			len = 1;
			break;
		case TW_MR_DATA_ACK:
			break;
		default:
			twierrno = 6;
			goto stop;
		}
		*out++ = TWDR;
		bytes++;
	}
	twierrno = 0;

stop:
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
quit:
	return bytes;
}

unsigned char twi_eeprom_write_bytes(
	const unsigned char addr,
	const unsigned char reg,
	unsigned char len,
	const unsigned char *in)
{
	unsigned char bytes = 0;
	if (twi_eeprom_select_register(addr, reg) != 0)
		return bytes;

	for (; len > 0; len--) {
		TWDR = *in++;
		TWCR = (1<<TWINT) | (1<<TWEN);
		while (!(TWCR & (1<<TWINT)))
			;
		switch (TW_STATUS) {
		case TW_MT_DATA_ACK:
			break;
		default:
			goto quit;
		}
		bytes++;
	}

quit:
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
	return bytes;
}
