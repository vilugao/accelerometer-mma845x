#include <stdio.h>
#include <string.h>
#include <math.h>

#include "accel_util.h"

double get_magnitude(const struct xyz_g *data)
{
	const double x = data->x;
	const double y = data->y;
	const double z = data->z;
	return sqrt(x * x + y * y + z * z);
}

void raw_accel_to_g(struct xyz_g *g_data, const struct xyz_raw *raw_data,
		const int fullscale_g)
{
	const double factor = fullscale_g / 2048.0;
	g_data->x = raw_data->x * factor;
	g_data->y = raw_data->y * factor;
	g_data->z = raw_data->z * factor;
}

int poll_accel(struct xyz_raw *raw_data, FILE * const serialin)
{
	char data[16];
	unsigned int x, y, z;
	int read;

	if (fgets(data, 16, serialin) == NULL)
		return 0;

	if (data[0] != '$') {
		fputs(data, stdout);
		return 0;
	}

	read = strlen(data);
	while (read < 14) {
		if (fgets(data + read, 16 - read, serialin) == NULL
				&& ferror(serialin))
			return -1;
		read = strlen(data);
	}

	if (0) {
		fputs("#INFO: Got it: \"", stdout);
		fputs(data, stdout);
		putchar('"');
		putchar('\n');
	}

	read = sscanf(data, "$%03X %03X %03X\r\n", &x, &y, &z);
	if (read < 3)
		return read;
	raw_data->x = (int)(signed char)(x >> 4) << 4 | (x & 0xF);
	raw_data->y = (int)(signed char)(y >> 4) << 4 | (y & 0xF);
	raw_data->z = (int)(signed char)(z >> 4) << 4 | (z & 0xF);

	return read;
}
