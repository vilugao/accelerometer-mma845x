#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "accel_util.h"

static FILE *accelinput;

static int width = 800;
static int height = 600;

static struct xyz_raw xyzvalues_raw;
static struct xyz_g xyzvalues_g;

static int parse_args(int argc, const char *argv[])
{
	if (argc >= 2) {
		accelinput = fopen(argv[1], "rb");
		if (accelinput == NULL) {
			perror(argv[1]);
			return 1;
		}
	}
	return 0;
}

static int read_accel(void)
{
	const int read = poll_accel(&xyzvalues_raw, accelinput);
	if (read < 3) {
		if (ferror(accelinput))
			return -1;
		return 0;
	}

	raw_accel_to_g(&xyzvalues_g, &xyzvalues_raw, 2);

	printf(
	"\r{%+5hd, %+5hd, %+5hd} {%+5.2f, %+5.2f, %+5.2f} = %+5.2f.",
		xyzvalues_raw.x, xyzvalues_raw.y, xyzvalues_raw.z,
		xyzvalues_g.x, xyzvalues_g.y, xyzvalues_g.z,
		get_magnitude(&xyzvalues_g));

	return read;
}

static void paint(void)
{
	ALLEGRO_COLOR black_color = al_map_rgb(0, 0, 0);
	ALLEGRO_COLOR green_color = al_map_rgb(0, 255, 0);
	ALLEGRO_COLOR blue_color = al_map_rgb(0, 0, 255);
	ALLEGRO_COLOR ball_color = al_map_rgb(
		xyzvalues_g.z < 0.0 ? 128 : 255, 0, 0);

	const float midx = width / 2.f;
	const float midy = height / 2.f;
	const float radius = height / 4.f;

	al_clear_to_color(black_color);

	al_draw_line(0, midy, width, midy, green_color, 0);
	al_draw_line(midx, 0, midx, height, green_color, 0);

	al_draw_circle(midx, midy, radius, blue_color, 0);

	al_draw_filled_circle(
		+xyzvalues_g.x * radius + midx,
		-xyzvalues_g.y * radius + midy,
		fabs(xyzvalues_g.z * 5.f) + 1.f, ball_color);

	al_flip_display();
}

int main(int argc, const char *argv[])
{
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *event_queue;
	ALLEGRO_EVENT event;

	accelinput = stdin;

	if (parse_args(argc, argv) != 0)
		return EXIT_FAILURE;

	if (!al_init())
		goto error;

	al_init_primitives_addon();

	display = al_create_display(width, height);
	if (display == NULL)
		goto error;
	al_set_window_title(display, "Accelerometer test");

	paint();

	event_queue = al_create_event_queue();
	al_register_event_source(event_queue,
		al_get_display_event_source(display));

	while (1) {
		if (read_accel() > 0)
			paint();

		if (!al_get_next_event(event_queue, &event))
			continue;
		if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			break;
	}

	puts("\nQuiting...");

	al_destroy_event_queue(event_queue);
	al_destroy_display(display);

	fclose(accelinput);
	exit(EXIT_SUCCESS);
error:
	exit(EXIT_FAILURE);
}
